# Odyzeo Guidelines

*Odyzeo company guidelines for better a more effective work flow*

## Table of Contents
1. [Project](#project)
	- [Tasks](#tasks)
	- [Git](#git)
	- [Database](#database)
	- [Infrastructure](#infrastructure)
	- [Deployment](#deployment)
	- [Code review](#code-review)
	- [SEO](#seo)
1. [Design](#design)
	- [Design guidelines](#design-guidelines)
	- [Images](#images)
	- [Resolutions](#resolutions)
1. [CSS](#3css)
    - [Principles](#css-coding-principles)
	- [Css guidelines](#css-guidelines)
	- [Icons](#icons)
	- [OOCSS and BEM](#oocss-and-bem)
	- [Atomic design](#atomic-design)
	- [Less and Sass](#less-and-sass)
	- [Media-queries](#media-queries)
	- [Hacks](#css-hacks)
1. [JavaScript](#javascript)
	- [JavaScript guidelines](#javascript-guidelines)
1. [Vue](#vue)
	- [Vue guidelines](#vue-guidelines)
1. [HTML](#html)
	- [HTML basics](#html-basics)
	- [HTML guidelines](#html-guidelines)
	- [Meta tags](#meta-tags)
1. [Blog](#blog)
    - [Blog guidelines](#blog-guidelines)
1. [PHP](#php)
    - [Laravel](#laravel)

# Project
Starting new project.
Have to be in [Teamwork](https://teamwork.com).
Must have clear roles (responsibilities) in team.
Nice to have some deadlines.
Create new [git init](#1.2.3-git-init).

## Tasks
Task 420 - Add new header

Take task from **To Do**.
If you have no task you have nothing to do.
Ask PM or create own task.
From **To Do** change state to **In progress**.
Every task is a new branch from **develop**  branch.
You can commit small changes and fixes directly to **develop**. Never commit to **master** branch, use it only for marge and have always code that is in production.

## Git
We are using [git](https://git-scm.com/) as distributed version control system.
You can use PhpStorm for git UI - Patrik will be happy to help you.

### ssh keys
Generate your SSH key with command with empty passphrase (will ask):
```shell
ssh-keygen -t ed25519 -C "your_email@odyzeo.com"
```

This will generate 2 files
```text
~/.ssh/id_ed25519
~/.ssh/id_ed25519.pub
```

Send `~/.ssh/id_ed25519.pub` to Patrik or Peter Š. and ask for permission to git.

### git aliases
In your ssh config file `~/.ssh/config` you can set aliases.

```text
Host zet
    Hostname 192.168.192.108
    User root
    IdentityFile ~/.ssh/id_ed25519
```

### git init
Create new git from https://gitlab.odyzeo.com GUI (only admin - @zombi, @pe4k, @dikant)
Git name is the same as project name lowercased dash-separated.

### git clone
Clone new created repository
```shell
$ git clone git@gitlab.odyzeo.com:odyzeo/coofee-shop.git
```

### git branches
If it's new project create develop branch. Take easy way and use UI, or use oldscool commands:
```shell
$ git checkout master
$ git pull
$ git checkout -b develop
```

Always create new branch from actual develop. Use UI or commands:
```shell
$ git checkout develop
$ git pull

// Create new feature branch from develop preferbly with some kind of descriptive addition
$ git checkout -b feature/420-some-descriptive-title

// Create new fix branch from develop
$ git checkout -b fix/713
```

### git commits
Commit messages contain clear description of the change in the present tense.
Start with task number in brackets 1 space and message starting capital letter without dot.

GOOD
```shell
$ git commit -m "[420] Add new header"
$ git commit -m "[713] Fix mobile menu icon"
$ git push
```

BAD
```shell
$ git commit -m "Added new header"
$ git commit -m "420Add header"
$ git commit -m "200-Add new header"
$ git commit -m "fix"
```

**Always Push your branch to remote**

**At the and of day push all your work**

If you have WIP - Work In Progress push only if someone ask you to.

After task is done merge your branch to develop and master if it's ready to deploy.
Always merge with `no fast forward` (`--no-ff`) option.
Than delete your branch locally and also from remote. UI has merging much easier, but you can use commands:

```shell
$ git checkout develop

// Always pull from remote before push
$ git pull develop

// Merge branch wih no forward option
$ git merge --no-ff feature/420

// Push your changes to remote
$ git push develop

// Delete branch locally
$ git branch -d feature/420

// Delete branch from remote
$ git push origin :feature/420
```

You can check if your branch is merged or not into current branch.

```shell
$ git branch --merged
$ git branch --no-merged
```

Ask for [code review](#1.4-code-review).

## Database
Create new empty PostgreSQL database. Ask @dikant.

## npm package manager
Since `npm 6.6.6` version we use `npm` instead `yarn`.

Upgrade to newest `npm` version:
```bash
npm i -g npm
```
When starting new project:
```bash
npm i
```
If you want to install dependencies in existing project which has `package-lock.json` use clean install:
```bash
npm ci
```
RIP `yarn`

## Deployment
Deploy test version on odyzeoclients.com into sub folder with same name as git repository.

```
/app
	/coffee-shop/
/sub
	/coffee-shop/
```

## Infrastructure

### Black magic
- passbolt (from outside with VPN)
- git https://gitlab.odyzeo.com (from outside with VPN)
- databases (no VPN needed)
```dotenv
DB_CONNECTION=pg
DB_HOST=hq.odyzeo.com
DB_PORT=2344
DB_USER=
DB_PASSWORD=
DB_DATABASE=
```

### Old debian
- databases - DEPRACATED - migrate to **Black magic**
```dotenv
DB_CONNECTION=pg
DB_HOST=192.168.192.109
DB_PORT=5433
DB_USER=
DB_PASSWORD=
DB_DATABASE=
```

### ZET
- backup gitlab
- backup passbolt
- projects data
- old VPN

### Forpsi dev server *dev.odyzeo.com
- http://dobrajazda.dev.odyzeo.com/

### VPN
1. IP: hq.oydzeo.com
1. Type: L2TP/IPsec with pre-shared key
1. Pre-shared key: NcjySXwg68df9jwY?MM73r93!Mku$vhQ
1. Login and password: same as for ZET

More info in [TeamWork notebook](https://odyzeo.teamwork.com/#notebooks/135386)

### PHP Storm
You can deploy directly from PhpStorm to FTP, FTPS or SFTP.
Go to `Project settings -> Build, Execution, Deployment -> Deployment`, press `+` to add new server. Then set connection to server and set correct file mapping.
Try to use secure connection always when you can (FTPS, SFTP). Use FTP only when you are sure another options are not available.

To deploy directories and files select them in PHPStorm, right click on it, and select `Deployment -> Sync with deployer to ...` and select your connection.
If right click dont have Deployment option (e.g. you want deploy files from some last commit), you can use `Tools -> Deployment -> Sync with deployer to ...` from topbar.

Always check all files changed on remote host and then click `Synchronize` or `Synchronize all` arrows on top.

### Laravel
Project running on VPS are deployed via [PHP Deployer](https://deployer.org/).
When VPS is ready, ask Patrik to create deployment script and read project's `readme.md` for more info.
Projects deployed with this deployer have possibility to rollback deploys up to `x` deploys.
Deployer always deploy `master` branch so make sure it is pushed.

## Code review
TODO PATRIK PETER

## SEO
TODO MIRO NINA

# Design
TODO OLEG DOKI PETER PATRIK

## Design guidelines
TODO

### Think mobile first

Do not just put everything on desktop and them remove elements. Also think on tablets. Think on mobile usability:
* Do not put CTA elements on the bottom of the page.
  * iPhones first show navigation bar when clicking on the bottom of the page.
* iPhones zoom in when input font-size is smaller than 16px.

### Fonts

* [Use minimum 16px size for main content](https://www.google.sk/search?q=best+page+content+font+size).
* Never use line height < 1.15 - safari will crop fonts on animations.
* Use as little font families and weights as possible.
  * It will speed-up page loading and prevent blinking fonts on page.
  * Google recommends to use optimally 2, maximally 5 fonts.
  * Every font family, font weight or italic is new font.
* Slovak pages need **Latin Extended** fonts so think of it while choosing fonts.
  * [`Lato` font on google fonts don't have some Slovak characters](https://github.com/google/fonts/issues/1564).
    * When you really want to use it, make sure developer use full [Lato font](http://www.latofonts.com/).
* Try to find your font in Google Fonts.
  * Google Fonts use CDN which serve fonts faster than common servers.
* If you do not like any fonts from Google Fonts provide formats:
  * **TTF/OTF** need always
  * **WOFF** nice to have
  * **WOFF2** best to have

### Zeplin

* Use **zeplin**/**sketch** application for designing.
* Always check your designs before exporting them and sharing link to project.
  * Final designs will be 1:1 to result page. Changes are acceptable while improving design/usability.
* **Do not apply any effects** that can't be used on web like `overlay: mulitple` etc.
* **Double check** if all images, icons and assets are exportable.
* Designer is responsible for delivering assets in such quality that developers do not have to crop images, fit icons etc.
* Don't use line height for arranging items, use multiple text elements.
  * Developers can't get right padding.
  * Developers can't get right space between elements (listing, paragraphs).
  * Elements can have more than 1 line of text, always think of it and set properly line height.

### User Experience

* Red color is really bad idea for submitting, confirming or saving buttons.
  * Red CTA when trying to sell some shitty product is OK.
* Think of the application with more states.
  * There are some error, info and warning states. Mostly forms have some validations - think of it and style them. If you need some **messages** ask PM or someone - really do not leave it to developer :).
  * Forms and some actions can be asynchronous. Design loading state on all async actions.
  * Buttons, links, inputs have also `:active`, `:hover`, `:focus` and links also `:visited` states.
  * Loading state not only on first page.

## Images
TODO

## Resolutions
TODO

# CSS

## CSS Coding Principles
- Separate layout, styles and logic
    - Not a lot of elements in block
    - Short files of JavaScript, CSS, Vue, PHP etc. - Keep it simple
- Do one thing and one thing only
    - Function, Object, Class, Block should do one thing and do it right
    - Design Interface first - Imagine what do you want from component to do?
- Naming
    - Take a time to name your variable/function so it will be understandable for other team member IMMEDIATELY
    - prefix function with their purpose: do, is, get, set, toggle, update, delete, trigger...
- Mobile first
    - per component / block / element -> not per template
    - focus on styling component globally then put him in layout
- Define global styles first
    - HTML5 tags h1..h6, ul, li, p, quote, a ...
    - You can use .h1, .h2, .h3 classes so you can keep semantic and add style
- ALWAYS check/clean/review your code before PUSH
    - no console
    - clean code
    - lint code
    - remove TODOs - when done
    - remove comments
    - LIKE IT WAS GOING LIVE

## CSS Guidelines

##### 1.1.1 No inline CSS.

##### 1.1.1 No !important. Only when using helpers with only one purpose.
```less
.text-right { text-align: right !important; }
```

##### 1.1.1 No units in zero values (there are some exceptions).
```less
.no-margin {
    margin: 0;
}
```

##### 1.1.1 Zero values exceptions
These values can't be used without units, otherwise it will break css.
Check the following twitter thread for discussion.
https://twitter.com/wesbos/status/1098658477935902720

```
perspective: 0;
transition: all 0; // or anything to do with timing
background: hsla(0, 0%, 100%, 0.5); // 0% can't be passed as 0
font-style: oblique 0; // needs 0deg
```

Also much of calc() breaks with unit-less 0.

##### 1.1.1 Prefer numbers instead of word.
**Bad**

```less
.font {
    font-weight: bold;
    letter-spacing: normal;
}
```

**Good**

```less
.font {
    font-weight: 700;
    letter-spacing: 0;
}
```

### 1.1.1 Rule declaration

A “rule declaration” is the name given to a selector (or a group of selectors) with an accompanying group of properties. Here's an example:

```less
.listing {
    font-size: 18px;
    line-height: 1.2;
}
```

### 1.1.1 Selectors

In a rule declaration, “selectors” are the bits that determine which elements in the DOM tree will be styled by the defined properties. Selectors can match HTML elements, as well as an element's class, ID, or any of its attributes. Here are some examples of selectors:

```less
.my-element-class {
    /* ... */
}

[aria-hidden] {
    /* ... */
}
```

### 1.1.1 Properties

Finally, properties are what give the selected elements of a rule declaration their style. Properties are key-value pairs, and a rule declaration can contain one or more property declarations. Property declarations look like this:

```less
/* some selector */ {
    background: #f1f1f1;
    color: #333;
}
```

##### 1.1.1 Ordering properties

- Layout Properties (position, z-index, float, clear, display)
- Box Model Properties (width, height, margin, padding)
- Visual Properties (color, background, border, box-shadow)
- Typography Properties (font-size, font-family, text-align, text-transform)
- Misc Properties (cursor, overflow)

##### 1.1.1 Order properties clockwise
Order properties clockwise. It means top-right-bottom-left.

##### 1.1.1 Always set z-index
Always set z-index when absolute|fixed positioning and put it after top|right|bottom|left so it's in one place.
Default z-index is auto, computed from parent which can be unnecessarily complicated and misleading.

##### 1.1.1 Always set horizontal and vertical values
Always set horizontal and vertical values when using absolute or fixed positioning.

**Nice**

```less
.something-absolute {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1;

    color: red;
    ...
}
```

**Bad**

```less
.something-absolute {
    position: absolute;
    width: 100%;
    left: 150px;
}
```

**Good**

```less
.something-absolute {
    position: absolute;
    top: 0;
    left: 150px;
    z-index: 0;

    width: 100%;
}
```

##### 1.1.1 Group related properties together if possible

**Bad**

```less
.something-absolute {
    display: flex;
    margin-top: 4px;
    flex-flow: column nowrap;
    padding: 10px;
    align-self: flex-start;
}
```

**Good**

```less
.something-absolute {
    display: flex;
    flex-direction: column;
    align-self: flex-start;

    margin-top: 4px;
    padding: 10px;
}
```

##### 1.1.1 Do not add redundant properties

**Bad**

```less
.something-flex {
    display: flex;
    flex-flow: row nowrap; // row and nowrap are initial values for flex
}
```

**Good**

```less
.something-flex {
    display: flex;
}
```

##### 1.1.1 Style element first as a child than as parent
For example in flex, if element is both **flex container** and **flex item** style it as **flex child** first.
Do not mix item and container styles, this is just because of logical structure and readability.

**Bad**

```less
.something-flex {
    display: flex;
    align-items: center;
    flex: 1 0 auto;
    justify-content: center;
}
```

**Good**

```less
.something-flex {
    // This applies to flex item
    flex: 1 0 auto;
    // This applies to flex container
    display: flex;
    align-items: center;
    justify-content: center;
}
```

### 1.1.1 Formatting

* Use soft tabs (4 spaces) for indentation.
* Use dashes class names. **No cameCase**.
  - Underscores are okay if you are using BEM (see [OOCSS and BEM](#3.13-oocss-and-bem) below).
* Do not use ID selectors.
* When using multiple selectors in a rule declaration, give each selector its own line.
* Put a space before the opening brace `{` in rule declarations.
* In properties, put a space after, but not before, the `:` character.
* Put closing braces `}` of rule declarations on a new line.
* Put blank lines between rule declarations.
* Use single quotes '' instead of doule "".

**Bad**

```less
.avatar{
    border-radius:50%;
    border:2px solid white; }
.no, .nope, .not_good {
    // ...
}
.veryUgly,
#lol-no {
    // ...
}
```

**Good**

```less
.avatar {
    border-radius: 50%;
    border: 2px solid white;
}

.one,
.selector,
.per-line {
    // ...
}
```

##### 1.1.1 Comments

* Prefer line comments (`//` in Less|Sass-land) to block comments.
* Prefer comments on their own line. Avoid end-of-line comments.
* Write detailed comments for code that isn't self-documenting:
  - Uses of z-index
  - Compatibility or browser-specific hacks

##### 1.1.1 ID selectors

While it is possible to select elements by ID in CSS, it should generally be considered an anti-pattern. ID selectors introduce an unnecessarily high level of [specificity](https://developer.mozilla.org/en-US/docs/Web/CSS/Specificity) to your rule declarations, and they are not reusable.

For more on this subject, read [CSS Wizardry's article](http://csswizardry.com/2014/07/hacks-for-dealing-with-specificity/) on dealing with specificity.

### 1.1.1 Element selectors

Do not use element selectors div, a, ul, li etc as well as '>'.

**Bad**

```less
.avatar a {
    color: #fff;
}
```

**Good**

```less
.avatar {

}

.avatar__link {
    color: #fff;
}
```

### 1.1.1 Colors

If posssile use 3 hexadecimals. Do not use default color names, instead find hexadecimal value. Prefer variables if suitable.

**Bad**

```less
.link {
    color: red;
    background-color: #ffffff;
}
```

**Good**

```less
.link {
    color: #f00;
    background-color: #fff;
}
```

**Best**

```less
.link {
    color: @color--danger;
    background-color: #fff;
}
```

### 1.1.1 Autoprefixer

We use autoprefixer so do not use vendor prefixes.

**Bad**

```less
.square {
    -webkit-transform: rotate(45deg);
    -moz-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    -o-transform: rotate(45deg);
    transform: rotate(45deg);
}
```

**Good**

```less
.square {
    transform: rotate(45deg);
}
```

### 1.1.1 JavaScript hooks

Avoid binding to the same class in both your CSS and JavaScript. Conflating the two often leads to, at a minimum, time wasted during refactoring when a developer must cross-reference each class they are changing, and at its worst, developers being afraid to make changes for fear of breaking functionality.

We recommend creating JavaScript-specific classes to bind to
  * prefixed with `.js-`.
  * using data attributes
  * using framework specific attributes

```html
<button class="btn btn-primary js-request-to-book">Request to Book</button>
<button class="btn btn-primary" @click.prevent="onClick()">Request to Book</button>
<button class="btn btn-primary" ng-click="onClick()">Request to Book</button>
```

### Icons
TODO SVETO icomoon

### OOCSS and BEM

// TODO

We encourage some combination of OOCSS and BEM for these reasons:

  * It helps create clear, strict relationships between CSS and HTML
  * It helps us create reusable, composable components
  * It allows for less nesting and lower specificity
  * It helps in building scalable stylesheets

**OOCSS**, or “Object Oriented CSS”, is an approach for writing CSS that encourages you to think about your stylesheets as a collection of “objects”: reusable, repeatable snippets that can be used independently throughout a website.

  * Nicole Sullivan's [OOCSS wiki](https://github.com/stubbornella/oocss/wiki)
  * Smashing Magazine's [Introduction to OOCSS](http://www.smashingmagazine.com/2011/12/12/an-introduction-to-object-oriented-css-oocss/)

**BEM**, or “Block-Element-Modifier”, is a _naming convention_ for classes in HTML and CSS. It was originally developed by Yandex with large codebases and scalability in mind, and can serve as a solid set of guidelines for implementing OOCSS.

  * CSS Trick's [BEM 101](https://css-tricks.com/bem-101/)
  * Harry Roberts' [introduction to BEM](http://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/)

Underscores and dashes are used for modifiers and children.

**Example**

```html
<article class="card card--featured">

  <header class="card__header">
    <h1 class="card__title">Adorable 2BR in the sunny Mission</h1>
    <div class="card__meta">Written by Zombi</div>
  </header>

  <div class="card__content">
    <p>Vestibulum id ligula porta felis euismod semper.</p>
  </div>

  <footer class="card__footer">
    <time class="card__date">10.07.2018</time>
  </footer>

</article>
```

```less
/* card.less */
.card { }
.card__header { }
.card__title { }
.card__meta { }
.card__content { }
.card--featured { }
```

  * `.card` is the “block” and represents the higher-level component
  * `.card__title` is an “element” and represents a descendant of `.card` that helps compose the block as a whole.
  * `.card--featured` is a “modifier” and represents a different state or variation on the `.card` block.

### Atomic design

// TODO http://bradfrost.com/blog/post/atomic-web-design/

### Less and Sass

##### 1.1.1 Syntax

* Use the `.less`|`.scss` syntax, never the original `.sass` syntax
* Order your regular CSS and `mixins`|`@include` declarations logically (see below)
* Always use nesting if needed - no more then one selector per line.

##### 1.1.1 Ordering of property declarations

1. Property declarations

    List all standard property declarations, anything that isn't an `@include` or a nested selector.

    ```less
    .btn-green {
        background: green;
        font-weight: 700;
        // ...
    }
    ```

2. `@include` declarations

    Grouping `@include`s at the end makes it easier to read the entire selector.

```less
.btn-green {
    background: green;
    font-weight: 700;
    // in Less
    .transition(background .5s ease);
    // in Sass
    // @include transition(background .5s ease);
    // ...
}
```

3. Nested selectors

    Nested selectors, _if necessary_, go after current selector state or class.
    Then you can add states and media queries last, nothing goes after them.
    Add whitespace between your rule declarations and nested selectors, as well as between adjacent nested selectors.
    Apply the same guidelines as above to your nested selectors.

```less
.btn {
    background: green;
    font-weight: bold;
    .transition(background .5s ease);

    &:hover,
    &:active {
        background: darken(green);
    }

    .icon {
        margin-right: 10px;
    }

    @media (min-width: @mobile-min-width) {
        padding: 4px;
    }
}
```

##### 1.1.1 Variables

Always create a file with variables. This file will contain:
* Main colors - best exported from zeplin.
  * You can use grays without variable, becase our designers love 50+ shades of gray,
* Breakpoints for both min and max use
```less
@break-large-min: 1440px; // to use with @media (min-width: @break-large-min)
@break-large-max: (@break-large-min - 1); // to use with @media (max-width: @break-large-max)
@break-desktop-min: 1024px;
@break-desktop-max: (@break-desktop-min - 1);
@break-tablet-min: 768px;
@break-tablet-max: (@break-tablet-min - 1);
@break-mobile-min: 425px;
@break-mobile-max: (@break-mobile-min - 1);
```
* All z-index
  * So developer can see it on one place and create new one with overlapping right elements.
* Everything you will use multiple times

Use dash-cased variable names (e.g. `$my-variable`). It is acceptable to prefix variable names that are intended to be used only within the same file with an underscore (e.g. `$_my-variable`).

##### 1.1.1 Mixins

Mixins should be used to DRY up your code, add clarity, or abstract complexity--in much the same way as well-named functions. Mixins that accept no arguments can be useful for this, but note that if you are not compressing your payload (e.g. gzip), this may contribute to unnecessary code duplication in the resulting styles.

##### 1.1.1 Extend directive

`@extend` should be avoided because it has unintuitive and potentially dangerous behavior, especially when used with nested selectors. Even extending top-level placeholder selectors can cause problems if the order of selectors ends up changing later (e.g. if they are in other files and the order the files are loaded shifts). Gzipping should handle most of the savings you would have gained by using `@extend`, and you can DRY up your stylesheets nicely with mixins.

##### 1.1.1 Nested selectors

**Do not nest selectors more than three levels deep!**

```less
.page-container {
    .content {
        .profile {
            // STOP!
        }
    }
}
```

When selectors become this long, you're likely writing CSS that is:

* Strongly coupled to the HTML (fragile) *—OR—*
* Overly specific (powerful) *—OR—*
* Not reusable

Again: **never nest ID selectors!**

If you must use an ID selector in the first place (and you should really try not to), they should never be nested. If you find yourself doing this, you need to revisit your markup, or figure out why such strong specificity is needed. If you are writing well formed HTML and CSS, you should **never** need to do this.

##### 1.1.1 Autofill e-mail/password
Webkit browsers autofill.

```
input:-webkit-autofill + label {
    // some styles for filled input label
}
```

1. Less functions

Use LESS functions instead custom computation.

**Bad**

```less
.square {
    flex-basis: 100/3%;
}
```

**Good**

```less
.square {
    flex-basis: percentage(1/3);
}
```

## Media queries
TODO PATRIK SVETO

## CSS Hacks
Fix ios input shadow and input type number arrows
```css
input, textarea {
    appearance: none;
    outline: none;
    background-clip: padding-box;
}

input[type="number"] {
    appearance: textfield;
}

input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    appearance: none;
    margin: 0;
}
```

# JavaScript

Using PHPStorm you can apply `.eslint` rules in `.eslint` file with right-click inside file and
selecting `Apply ESLint Code Style Rules`.

We are using [Vue recommended eslint plugin](https://github.com/vuejs/eslint-plugin-vue/tree/master/docs/rules)
with few adjustments. See [package.example.json](package.example.json)

## JavaScript Guidelines
In your code use [JSDoc](http://usejsdoc.org/) for documentation.
It helps you think better what should function do, if you can't doc it you have to think more
what you function should/should not do. Functions should do **one thing and one thing only and do it well**.

### JavaScript Components
When creating new component use simple clear purpose for its name.
Create new JS and CSS file with lowercase dash-separated name exactly like component name.

```
/js
  /components
    contact-panel.vue
    ...

/css
  /components
    contact-panel.less
    ...
```

### 1.1.1 Mixing plain JS with Frameworks
If it's very very simple app use just plain JS. When there is more complicated logic use Vue or appropriate JS framework.
If there is some framework in project already use it.
Try not to mix jQuery, Vue, Angular and plain JS in one project.

### 1.1.1 No console
Do not push your debugging `console.log` statements unless they are helpful for development.

**Bad**

```js
console.log('hello', element)
```

**Better**

```js
console.log('ACTION onInit', value)
```

### 1.1.1 Multiple attributes
If there are more than 1 attribute in component on element use one per line ending with `>` on new line
(in git there is just one line changed).
Empty elements close in one line.
Longer texts in elements divide in new line.
Read Vue styleguides: https://vuejs.org/v2/style-guide/#Multi-attribute-elements-strongly-recommended.
Use Vue recommended attributes order: https://vuejs.org/v2/style-guide/#Element-attribute-order-recommended.

**Bad**

```html
<div class="element" v-for="e in elements"
     :key="e.id">
    Hello
</div>

<div>
</div>

<div>{{ element }} and {{ longVariable }} and some text between and Lorem impsin with <span>span</span> etc</div>
```

**Good**

```html
<div
    class="element"
    :key="e.id"
    v-for="e in elements"
>
    Hello
</div>

<div></div>

<div>
    {{ element }} and {{ longVariable }} and some text between and Lorem impsin with
    <span>span</span>
    etc
</div>
```

### 1.1.1 Always use parenthesis after if condition
Good for readability.

**Bad**
```js
if (typeof value === 'undefined') return value
```

**Good**
```js
if (typeof value === 'undefined') {
    return value
}
```

# Vue

## Vue Guidelines
We follow [Official Vue Style Guide](https://vuejs.org/v2/style-guide/).
Points where we have different convenction are listed below:

### [Best Component name casing in templates](https://vuejs.org/v2/style-guide/#Component-name-casing-in-templates-strongly-recommended)
Because we use Vue also in Laravel projects where components can be inside `*.blade.php` files
it's nice to have unique syntax, wile `<kebab-case></kebab-case>` is recommended in DOM templates.

**Good**
```
<my-component><my-component/>
```

**Bad**
```
<MyComponent><MyComponent/>
<MyComponent/>
<my-component/>
```

### [Element attribute order](https://vuejs.org/v2/style-guide/#Element-attribute-order-recommended)

We prefer native attributes first, then order like in Vue style guide. Open to discussion.

**Good**
```
<my-component
    class="form-item"
    :class="someGeneratedClass"
    v-if="something"
t><my-component/>
```

**Bad**
```
<my-component
    v-if="something"
    :class="someGeneratedClass"
    class="form-item"
t><my-component/>
```

# HTML

## HTML Basics

### 1.1.1 Encode HTML entities

**Bad**

```html
100€
Rick & Morty
```

**Good**

```html
100&euro;
Rick &amp; Morty
```

## HTML Guidelines
// TODO

## Meta tags
// TODO

# Blog

## Blog guidelines

### Blog title

- About ~50 characters long.
- Keyword on the beginning of the title.
- Use numbers in titles.
- Paraphrasing.
- Should contain verb.
- Using foreign words, generally known.

### Blog perex

- Length of 100 - 150 characters.
- Short, simple summary of the most important points in the article.
- Contains as many as possible of keywords.

### Blog content

- Length of 1000 - 1500 characters / 300-350 words.
- Divide the individual parts and create subtitles.
- Do not forget to mention source.
- Always add sources and captions to images.

# PHP

## Laravel

### Create migration
If you want to add **foo** column to **bar** table:
```shell
$ php artisan make:migration add_foo_column_to_bar_table --table=bar
```

Then change connection in .env file and run:
```shell
$ php artisan migrate
```
